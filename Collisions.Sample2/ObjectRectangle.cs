﻿using Collisions.Source.Implementation;
using Collisions.Source.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collisions.Sample2
{
    public class ObjectRectangle : ICollisionRectangle
    {
        public int Speed { get; set; } = 5;
        public Color Color { get; set; }
        public SpriteFont Font { get; set; }
        public Texture2D Texture { get; set; }
        public Point Size { get; set; }
        public Point Location { get; set; }
        public Rectangle Rectangle
        {
            get { return new Rectangle(Location, Size); }
            set
            {
                this.Size = value.Size;
                this.Location = value.Location;
            }
        }
        public ICollisionManager Collisions { get; set; } = CollisionManager.GetInstance();
        public bool HasCollision { get { return Collisions.FindRelations(this).Any(rel => rel.IsHaveCollision()); } }
        public string Message { get; set; }

        public ObjectRectangle(Texture2D texture, Color color, Rectangle rect, SpriteFont font)
        {
            this.Texture = texture;
            this.Color = color;
            this.Rectangle = rect;
            this.Font = font;
            this.Message = "";
        }

        public void Update(Keys left, Keys right, Keys up, Keys down)
        {
            KeyboardState state = Keyboard.GetState();

            if (state.IsKeyDown(up))
            {
                this.Location = new Point(this.Rectangle.X, this.Rectangle.Y - Speed);
            }

            if (state.IsKeyDown(down))
            {
                this.Location = new Point(this.Rectangle.X, this.Rectangle.Y + Speed);
            }

            if (state.IsKeyDown(left))
            {
                this.Location = new Point(this.Rectangle.X - Speed, this.Rectangle.Y);
            }

            if (state.IsKeyDown(right))
            {
                this.Location = new Point(this.Rectangle.X + Speed, this.Rectangle.Y);
            }

            this.Message = "Has Collision:" + Environment.NewLine +
                this.HasCollision.ToString() + Environment.NewLine +
                Environment.NewLine +
                "Controls:" + Environment.NewLine +
                "Left: " + left + Environment.NewLine +
                "Right: " + right + Environment.NewLine +
                "Up: " + up + Environment.NewLine +
                "Down: " + down;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(this.Texture, this.Rectangle, Color);
            spriteBatch.DrawString(this.Font, this.Message, Vector2.One + this.Location.ToVector2(), Color.LightGreen);
        }
    }
}
