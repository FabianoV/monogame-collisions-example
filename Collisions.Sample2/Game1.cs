﻿using Collisions.Source.Implementation;
using Collisions.Source.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Linq;

namespace Collisions.Sample2
{
    public class Game1 : Game
    {
        //Properties
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public Texture2D BrownTexture { get; set; } 
        public SpriteFont Font { get; set; }
        public string Description { get; set; } = "Sample shows how relation works in collision micro-framework";
        public string MessageControlRed { get; set; } = "Red controls: W,S,A,D";
        public string MessageControlOrange { get; set; } = "Red controls: U,J,H,K";
        public string MessageControlBlue { get; set; } = "Blue controls: Up, Down, Left, Right arrows";
        public string Message { get { return this.Description + "\n" + MessageControlBlue + "\n" + MessageControlRed + "\n" + MessageControlOrange; } }

        public ObjectRectangle Blue { get; set; }
        public ObjectRectangle Red { get; set; }
        public ObjectRectangle Orange { get; set; }

        public ICollisionManager Collisions { get; set; } = CollisionManager.GetInstance();

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this)
            {
                IsFullScreen = false,
                PreferredBackBufferWidth = 700,
                PreferredBackBufferHeight = 430
            };
            graphics.ApplyChanges();
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();

            this.BrownTexture = TextureCreatorHelper.CreateTexture(this.GraphicsDevice, 100, 100, Color.White);

            Texture2D defaultTexture = TextureCreatorHelper.CreateTexture(this.GraphicsDevice, 1, 1, Color.White); //Texture for mask color

            //Create objects
            this.Red = new ObjectRectangle(defaultTexture, Color.Red, new Rectangle(250, 170, 160, 240), this.Font);
            this.Blue = new ObjectRectangle(defaultTexture, Color.Blue, new Rectangle(20, 130, 200, 200), this.Font);
            this.Orange = new ObjectRectangle(defaultTexture, Color.Orange, new Rectangle(450, 110, 200, 200), this.Font);

            //Register collision relations
            this.Collisions.RegisterRelation(this.Red, this.Blue);
            this.Collisions.RegisterRelation(this.Red, this.Orange);
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            this.Font = this.Content.Load<SpriteFont>("ArialFont");
        }


        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            this.Red.Update(Keys.A, Keys.D, Keys.W, Keys.S);
            this.Blue.Update(Keys.Left, Keys.Right, Keys.Up, Keys.Down);
            this.Orange.Update(Keys.H, Keys.K, Keys.U, Keys.J);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            this.spriteBatch.Begin();
            //Draw Objects
            this.Red.Draw(spriteBatch);
            this.Blue.Draw(spriteBatch);
            this.Orange.Draw(spriteBatch);

            //Draw Text
            this.spriteBatch.DrawString(this.Font, this.Message, Vector2.One, Color.Red);
            this.spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
