﻿using Collisions.Source.Implementation.Strategy;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collisions.Source.Interfaces
{
    public interface ICollisionRelation : IDisposable
    {
        ICollisionObject Obj1 { get; }
        ICollisionObject Obj2 { get; }

        CollisionStrategyType CollisionStrategyType { get; }
        BaseCollisionStrategy CollisionStrategy { get; }

        bool IsHaveCollision();
        Rectangle GetIntersection();
        float GetCollisionArea();
    }
}
