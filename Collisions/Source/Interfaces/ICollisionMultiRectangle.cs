﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collisions.Source.Interfaces
{
    public interface ICollisionMultiRectangle : ICollisionObject
    {
        List<Rectangle> Rectangles { get; set; }
    }
}
