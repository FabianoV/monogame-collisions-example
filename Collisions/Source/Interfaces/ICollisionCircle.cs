﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collisions.Source.Interfaces
{
    public interface ICollisionCircle : ICollisionObject
    {
        Point Location { get; set; }
        float Radius { get; set; }
    }
}
