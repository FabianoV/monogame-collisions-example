﻿using Collisions.Source.Implementation.Strategy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collisions.Source.Interfaces
{
    public interface ICollisionManager : IDisposable
    {
        ICollisionRelation RegisterRelation(ICollisionObject obj1, ICollisionObject obj2, CollisionStrategyType strategy);
        ICollisionRelation RegisterRelation(ICollisionObject obj1, ICollisionObject obj2);
        ICollisionRelation FindRelation(ICollisionObject obj1, ICollisionObject obj2);
        List<ICollisionRelation> FindRelations(ICollisionObject obj);
    }
}
