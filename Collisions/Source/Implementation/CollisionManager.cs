﻿using Collisions.Source.Implementation.Strategy;
using Collisions.Source.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collisions.Source.Implementation
{
    public class CollisionManager : ICollisionManager, IDisposable
    {
        #region  Singleton
        public static CollisionManager Instance { get; private set; }
        public static ICollisionManager GetInstance()
        {
            Instance = Instance ?? new CollisionManager();
            return Instance;
        }
        #endregion

        //Properties
        public CollisionStrategyTypeChecker TypeChecker { get; set; }
        public CollisionStrategyFactory StrategyFactory { get; set; }
        public List<ICollisionRelation> Relations { get; set; }

        //Constructor
        private CollisionManager()
        {
            this.TypeChecker = new CollisionStrategyTypeChecker();
            this.StrategyFactory = new CollisionStrategyFactory();
            this.Relations = new List<ICollisionRelation>();
        }

        //Methods
        public ICollisionRelation RegisterRelation(ICollisionObject obj1, ICollisionObject obj2, CollisionStrategyType strategyType)
        {
            if (obj1 == null || obj2 == null)
            {
                throw new NullReferenceException("Any collision object can not be null.");
            }

            if (this.IsRelationDefined(obj1, obj2))
            {
                throw new CollisionExistsException(obj1, obj2);
            }

            BaseCollisionStrategy strategy = this.StrategyFactory.CreateStrategy(strategyType);

            ICollisionRelation relation = new CollisionRelation()
            {
                Obj1 = obj1,
                Obj2 = obj2,
                CollisionStrategy = strategy,
            };

            this.Relations.Add(relation);

            return relation;
        }
        public ICollisionRelation RegisterRelation(ICollisionObject obj1, ICollisionObject obj2)
        {
            CollisionStrategyType strategyType = this.TypeChecker.CheckCollisionTypeByObjectsInstances(obj1, obj2);
            return this.RegisterRelation(obj1, obj2, strategyType);
        }

        private bool IsRelationDefined(ICollisionObject obj1, ICollisionObject obj2)
        {
            if (this.FindRelation(obj1, obj2) == null)
            {
                return false;
            }

            return true;
        }
        public ICollisionRelation FindRelation(ICollisionObject obj1, ICollisionObject obj2)
        {
            foreach (ICollisionRelation relation in this.Relations)
            {
                if (relation.Obj1.Equals(obj1) && relation.Obj2.Equals(obj2))
                {
                    return relation;
                }

                if (relation.Obj2.Equals(obj1) && relation.Obj1.Equals(obj2))
                {
                    return relation;
                }
            }

            return null;
        }
        public List<ICollisionRelation> FindRelations(ICollisionObject obj)
        {
            List<ICollisionRelation> relations = new List<ICollisionRelation>();
            foreach (ICollisionRelation relation in this.Relations)
            {
                if (relation.Obj1.Equals(obj) || relation.Obj2.Equals(obj))
                {
                    relations.Add(relation);
                }
            }
            return relations;
        }

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.Relations = null;
            }
        }
        #endregion
    }
}
