﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collisions.Source.Implementation.Strategy
{
    public enum CollisionStrategyType
    {
        BoxToBox = 0,
        CircleToCircle = 1,
        PointToCircle = 2,
        PointToBox = 4,
        PointToPoint = 5,
        MultiBoxToMultiBox = 6,
    }
}
