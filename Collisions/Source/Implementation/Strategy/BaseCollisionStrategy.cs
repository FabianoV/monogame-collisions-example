﻿using Collisions.Source.Interfaces;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collisions.Source.Implementation.Strategy
{
    public abstract class BaseCollisionStrategy
    {
        //Properties
        public abstract CollisionStrategyType Type { get; }
        public abstract Type ExpectedObj1Type { get; }
        public abstract Type ExpectedObj2Type { get; }

        //Methods
        public bool IsHasCollision(ICollisionObject obj1, ICollisionObject obj2)
        {
            if (!this.IsObjectsAreValid(obj1, obj2))
            {
                //Swap objects
                ICollisionObject temp = obj2;
                obj2 = obj1;
                obj1 = temp;

                //Check swapped objects
                if (!this.IsObjectsAreValid(obj1, obj2))
                {
                    throw new Exception("CollisionStrategyType is not match to ICollisionObject types.");
                }
            } 

            return this.CalculateCollision(obj1, obj2);
        }
        public bool IsObjectsAreValid(ICollisionObject obj1, ICollisionObject obj2)
        {
            if (this.ExpectedObj1Type.IsAssignableFrom(obj1.GetType()) && this.ExpectedObj2Type.IsAssignableFrom(obj2.GetType()))
            {
                return true;
            }

            return false;
        }
        public abstract bool CalculateCollision(ICollisionObject obj1, ICollisionObject obj2);
        public abstract Rectangle GetIntersection(ICollisionObject obj1, ICollisionObject obj2);
        public abstract float GetCollisionArea(ICollisionObject obj1, ICollisionObject obj2);
    }
}
