﻿using Collisions.Source.Implementation.Strategy.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Collisions.Source.Implementation.Strategy
{
    public class CollisionStrategyFactory
    {
        public BaseCollisionStrategy CreateStrategy(CollisionStrategyType type)
        {
            switch (type)
            {
                case CollisionStrategyType.BoxToBox: return new BoxToBoxCollisionStrategy();
                case CollisionStrategyType.CircleToCircle: return new CircleToCircleCollisionStrategy();
                case CollisionStrategyType.PointToCircle: return new PointToCircleCollisionStrategy();
                case CollisionStrategyType.PointToBox: return new PointToBoxCollisionStrategy();
                case CollisionStrategyType.PointToPoint: return new PointToPointCollisionStrategy();
                case CollisionStrategyType.MultiBoxToMultiBox: return new MultiBoxToMultiBoxCollisionStrategy();
                default:
                    throw new NotImplementedException("Not recognized CollisionStrategyType.");
            }
        }
    }
}
