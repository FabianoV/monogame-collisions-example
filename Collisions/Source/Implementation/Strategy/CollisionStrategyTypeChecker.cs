﻿using Collisions.Source.Implementation.Strategy;
using Collisions.Source.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collisions.Source.Implementation.Strategy
{
    public class CollisionStrategyTypeChecker
    {
        public CollisionStrategyType CheckCollisionTypeByObjectsInstances(ICollisionObject obj1, ICollisionObject obj2)
        {
            if (obj1 is ICollisionRectangle && obj2 is ICollisionRectangle)
            {
                return CollisionStrategyType.BoxToBox;
            }

            if (obj1 is ICollisionCircle && obj2 is ICollisionCircle)
            {
                return CollisionStrategyType.CircleToCircle;
            }

            if ((obj1 is ICollisionPoint && obj2 is ICollisionRectangle) ||
                (obj1 is ICollisionRectangle && obj2 is ICollisionPoint))
            {
                return CollisionStrategyType.PointToBox;
            }

            if ((obj1 is ICollisionPoint && obj2 is ICollisionCircle) ||
                (obj1 is ICollisionCircle && obj2 is ICollisionPoint))
            {
                return CollisionStrategyType.PointToCircle;
            }

            if ((obj1 is ICollisionPoint && obj2 is ICollisionPoint))
            {
                return CollisionStrategyType.PointToPoint;
            }

            if (obj1 is List<ICollisionRectangle> && obj2 is List<ICollisionRectangle>)
            {
                return CollisionStrategyType.MultiBoxToMultiBox;
            }

            throw new NotSupportedException("Collision beetween objects is not possible. Bad obj type.");
        }
    }
}
