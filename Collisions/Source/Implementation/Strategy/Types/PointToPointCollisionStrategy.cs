﻿using Collisions.Source.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Collisions.Source.Implementation.Strategy.Types
{
    public class PointToPointCollisionStrategy : BaseCollisionStrategy
    {
        //Properties
        public override CollisionStrategyType Type { get { return CollisionStrategyType.PointToPoint; } }
        public override Type ExpectedObj1Type { get { return typeof(ICollisionPoint); } }
        public override Type ExpectedObj2Type { get { return typeof(ICollisionPoint); } }

        //Methods
        public override bool CalculateCollision(ICollisionObject obj1, ICollisionObject obj2)
        {
            ICollisionPoint point1 = obj1 as ICollisionPoint;
            ICollisionPoint point2 = obj2 as ICollisionPoint;

            if (point1.Location == point2.Location)
            {
                return true;
            }

            return false;
        }

        public override float GetCollisionArea(ICollisionObject obj1, ICollisionObject obj2)
        {
            if (this.CalculateCollision(obj1, obj2))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public override Rectangle GetIntersection(ICollisionObject obj1, ICollisionObject obj2)
        {
            ICollisionPoint point1 = obj1 as ICollisionPoint;
            ICollisionPoint point2 = obj2 as ICollisionPoint;

            if (this.CalculateCollision(obj1, obj2))
            {
                return new Rectangle(point1.Location, new Point(1, 1));
            }

            return Rectangle.Empty;
        }
    }
}
