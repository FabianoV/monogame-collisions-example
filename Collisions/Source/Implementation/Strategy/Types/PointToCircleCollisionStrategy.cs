﻿using Collisions.Source.Interfaces;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collisions.Source.Implementation.Strategy.Types
{
    public class PointToCircleCollisionStrategy : BaseCollisionStrategy
    {
        //Properties
        public override CollisionStrategyType Type { get { return CollisionStrategyType.PointToCircle; } }
        public override Type ExpectedObj1Type { get { return typeof(ICollisionPoint); } }
        public override Type ExpectedObj2Type { get { return typeof(ICollisionCircle); } }

        //Methods
        public override bool CalculateCollision(ICollisionObject obj1, ICollisionObject obj2)
        {
            ICollisionPoint point1 = obj1 as ICollisionPoint;
            ICollisionCircle circle2 = obj2 as ICollisionCircle;

            if (Vector2.Distance(point1.Location.ToVector2(), circle2.Location.ToVector2()) < circle2.Radius)
            {
                return true;
            }

            return false;
        }

        public override float GetCollisionArea(ICollisionObject obj1, ICollisionObject obj2)
        {
            throw new NotSupportedException("Intersection area is not supported in Point to Circle strategy.");
        }

        public override Rectangle GetIntersection(ICollisionObject obj1, ICollisionObject obj2)
        {
            throw new NotSupportedException("Intersection is not supported in Point to Circle strategy.");
        }
    }
}
