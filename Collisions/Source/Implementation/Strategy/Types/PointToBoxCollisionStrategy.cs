﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Collisions.Source.Interfaces;
using Microsoft.Xna.Framework;

namespace Collisions.Source.Implementation.Strategy.Types
{
    public class PointToBoxCollisionStrategy : BaseCollisionStrategy
    {
        //Properties
        public override CollisionStrategyType Type { get { return CollisionStrategyType.PointToBox; } }
        public override Type ExpectedObj1Type { get { return typeof(ICollisionPoint); } }
        public override Type ExpectedObj2Type { get { return typeof(ICollisionRectangle); } }

        //Methods
        public override bool CalculateCollision(ICollisionObject obj1, ICollisionObject obj2)
        {
            ICollisionPoint point1 = obj1 as ICollisionPoint;
            ICollisionRectangle rect2 = obj2 as ICollisionRectangle;

            if (point1.Location.X > rect2.Rectangle.X && point1.Location.X < (rect2.Rectangle.X + rect2.Rectangle.Width) &&
                point1.Location.Y > rect2.Rectangle.Y && point1.Location.Y < (rect2.Rectangle.Y + rect2.Rectangle.Height))
            {
                return true;
            }

            return false;
        }

        public override float GetCollisionArea(ICollisionObject obj1, ICollisionObject obj2)
        {
            if (this.CalculateCollision(obj1, obj2))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public override Rectangle GetIntersection(ICollisionObject obj1, ICollisionObject obj2)
        {
            ICollisionPoint point1 = obj1 as ICollisionPoint;
            ICollisionRectangle rect2 = obj2 as ICollisionRectangle;

            if (this.CalculateCollision(obj1, obj2))
            {
                return new Rectangle(point1.Location, new Point(1, 1));
            }

            return Rectangle.Empty;
        }
    }
}
