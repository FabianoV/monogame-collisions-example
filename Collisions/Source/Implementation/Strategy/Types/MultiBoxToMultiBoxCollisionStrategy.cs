﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Collisions.Source.Interfaces;
using Microsoft.Xna.Framework;

namespace Collisions.Source.Implementation.Strategy.Types
{
    public class MultiBoxToMultiBoxCollisionStrategy : BaseCollisionStrategy
    {
        //Properties
        public override CollisionStrategyType Type { get { return CollisionStrategyType.MultiBoxToMultiBox; } }
        public override Type ExpectedObj1Type { get { return typeof(List<ICollisionRectangle>); } }
        public override Type ExpectedObj2Type { get { return typeof(List<ICollisionRectangle>); } }

        public override bool CalculateCollision(ICollisionObject obj1, ICollisionObject obj2)
        {
            throw new NotSupportedException("Not possible in MultiBox collisions.");
        }

        public override float GetCollisionArea(ICollisionObject obj1, ICollisionObject obj2)
        {
            throw new NotSupportedException("Not possible in MultiBox collisions.");
        }

        public override Rectangle GetIntersection(ICollisionObject obj1, ICollisionObject obj2)
        {
            throw new NotSupportedException("Not possible in MultiBox collisions.");
        }
    }
}
