﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Collisions.Source.Interfaces;
using Microsoft.Xna.Framework;

namespace Collisions.Source.Implementation.Strategy.Types
{
    public class BoxToBoxCollisionStrategy : BaseCollisionStrategy
    {
        //Properties
        public override CollisionStrategyType Type { get { return CollisionStrategyType.BoxToBox; } }
        public override Type ExpectedObj1Type { get { return typeof(ICollisionRectangle); } }
        public override Type ExpectedObj2Type { get { return typeof(ICollisionRectangle); } }

        //Methods
        public override bool CalculateCollision(ICollisionObject obj1, ICollisionObject obj2)
        {
            ICollisionRectangle rect1 = obj1 as ICollisionRectangle;
            ICollisionRectangle rect2 = obj2 as ICollisionRectangle;

            return rect1.Rectangle.Intersects(rect2.Rectangle);
        }

        public override float GetCollisionArea(ICollisionObject obj1, ICollisionObject obj2)
        {
            Rectangle intersection = this.GetIntersection(obj1, obj2);
            float area = intersection.Width * intersection.Height;
            return area;
        }

        public override Rectangle GetIntersection(ICollisionObject obj1, ICollisionObject obj2)
        {
            ICollisionRectangle rect1 = obj1 as ICollisionRectangle;
            ICollisionRectangle rect2 = obj2 as ICollisionRectangle;

            return Rectangle.Intersect(rect1.Rectangle, rect2.Rectangle);
        }
    }
}
