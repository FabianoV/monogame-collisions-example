﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Collisions.Source.Interfaces;
using Microsoft.Xna.Framework;

namespace Collisions.Source.Implementation.Strategy.Types
{
    public class CircleToCircleCollisionStrategy : BaseCollisionStrategy
    {
        //Properties
        public override CollisionStrategyType Type { get { return CollisionStrategyType.CircleToCircle; } }
        public override Type ExpectedObj1Type { get { return typeof(ICollisionCircle); } }
        public override Type ExpectedObj2Type { get { return typeof(ICollisionCircle); } }

        //Methods
        public override bool CalculateCollision(ICollisionObject obj1, ICollisionObject obj2)
        {
            ICollisionCircle circle1 = obj1 as ICollisionCircle;
            ICollisionCircle circle2 = obj2 as ICollisionCircle;

            if (Vector2.Distance(circle1.Location.ToVector2(), circle2.Location.ToVector2()) < circle1.Radius + circle2.Radius)
            {
                return true;
            }

            return false;
        }

        public override float GetCollisionArea(ICollisionObject obj1, ICollisionObject obj2)
        {
            throw new NotSupportedException("Intersection area is not supported in Circle to Circle strategy.");
        }

        public override Rectangle GetIntersection(ICollisionObject obj1, ICollisionObject obj2)
        {
            throw new NotSupportedException("Intersection is not supported in Circle to Circle strategy.");
        }
    }
}
