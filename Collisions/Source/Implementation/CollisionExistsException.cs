﻿using Collisions.Source.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collisions.Source.Implementation
{
    public class CollisionExistsException : Exception
    {
        public ICollisionObject Obj1 { get; set; }
        public ICollisionObject Obj2 { get; set; }

        public CollisionExistsException(ICollisionObject obj1, ICollisionObject obj2)
            : base("Collision between objects has been defined earlier.")
        {
            this.Obj1 = obj1;
            this.Obj2 = obj2;
        }
    }
}
