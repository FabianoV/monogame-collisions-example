﻿using Collisions.Source.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Collisions.Source.Implementation.Strategy;

namespace Collisions.Source.Implementation
{
    public class CollisionRelation : ICollisionRelation
    {
        //Properties
        public ICollisionObject Obj1 { get; set; }
        public ICollisionObject Obj2 { get; set; }

        public CollisionStrategyType CollisionStrategyType { get; set; }
        public BaseCollisionStrategy CollisionStrategy { get; set; }

        //Constructor
        public CollisionRelation()
        {

        }

        //Methods
        public bool IsHaveCollision()
        {
            return this.CollisionStrategy.IsHasCollision(this.Obj1, this.Obj2);
        }

        public float GetCollisionArea()
        {
            return this.CollisionStrategy.GetCollisionArea(this.Obj1, this.Obj2);
        }

        public Rectangle GetIntersection()
        {
            return this.CollisionStrategy.GetIntersection(this.Obj1, this.Obj2);
        }

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.Obj1 = null;
                this.Obj2 = null;
            }
        }
        #endregion
    }
}
