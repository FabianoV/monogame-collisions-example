﻿using Collisions.Source.Implementation;
using Collisions.Source.Implementation.Strategy;
using Collisions.Source.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Linq;

namespace Collisions.Sample
{
    public class Game1 : Game
    {
        //Properties
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public string Description { get; set; } = "Sample shows how to calculate collision area by this collision micro-framework";
        public string MessageCollision { get; set; }
        public string MessageControlRed { get; set; } = "Red controls: W,S,A,D";
        public string MessageControlBlue { get; set; } = "Blue controls: Up, Down, Left, Right arrows";
        public string Message { get { return this.Description + "\n" + MessageCollision + "\n" + MessageControlBlue + "\n" + MessageControlRed; } }

        public ICollisionManager Collisions { get; set; }
        public ICollisionRelation RedBlueCollisionRelation { get; set; }

        public ObjectRectangle Blue { get; set; }
        public ObjectRectangle Red { get; set; }

        public Texture2D BrownTexture { get; set; }
        public SpriteFont Font { get; set; }

        //Constructor
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this)
            {
                IsFullScreen = false,
                PreferredBackBufferWidth = 800,
                PreferredBackBufferHeight = 400
            };
            graphics.ApplyChanges();
            Content.RootDirectory = "Content";
        }

        //Methods
        protected override void Initialize()
        {
            base.Initialize();

            this.BrownTexture = CreateTexture(this.GraphicsDevice, 100, 100, Color.Brown);

            this.Red = new ObjectRectangle()
            {
                Rectangle  = new Rectangle(130, 180, 80, 120), // Size of Blue Rect
                Texture = CreateTexture(this.GraphicsDevice, 100, 100, Color.Blue)
            };

            this.Blue = new ObjectRectangle()
            {
                Rectangle = new Rectangle(100, 150, 100, 100), //Size of Red Rect
                Texture = CreateTexture(this.GraphicsDevice, 100, 100, Color.Red)
            };

            this.Collisions = CollisionManager.GetInstance();
            this.RedBlueCollisionRelation = this.Collisions.RegisterRelation(this.Red, this.Blue, CollisionStrategyType.BoxToBox);
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            this.Font = this.Content.Load<SpriteFont>("ArialFont");
        }

        public static Texture2D CreateTexture(GraphicsDevice device, int width, int height, Color color)
        {
            Texture2D texture = new Texture2D(device, width, height);
            Color[] data = new Color[width * height];
            for (int pixel = 0; pixel < data.Count(); pixel++)
            {
                data[pixel] = color;
            }
            texture.SetData(data);
            return texture;
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            this.Red.Update(Keys.A, Keys.D, Keys.W, Keys.S);
            this.Blue.Update(Keys.Left, Keys.Right, Keys.Up, Keys.Down);


            if (this.RedBlueCollisionRelation.IsHaveCollision())
            {
                this.MessageCollision = "Red has collision with Blue. " + this.RedBlueCollisionRelation.GetCollisionArea() + " points of pixel area";
            }
            else
            {
                this.MessageCollision = "Red no has collision with Blue.";
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            this.spriteBatch.Begin();
            //Draw Objects
            this.spriteBatch.Draw(this.Blue.Texture, this.Blue.Rectangle, Color.White); //Blue object
            this.spriteBatch.Draw(this.Red.Texture, this.Red.Rectangle, Color.White); //Red object
            //this.spriteBatch.Draw(this.BrownTexture, this.RedBlueCollisionRelation.Intersection, Color.White); //Intersection

            //Draw Text
            this.spriteBatch.DrawString(this.Font, this.Message, Vector2.One, Color.Red);
            this.spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
