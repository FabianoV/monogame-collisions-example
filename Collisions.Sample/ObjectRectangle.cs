﻿using Collisions.Source.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collisions.Sample
{
    public class ObjectRectangle : ICollisionRectangle
    {
        public int Speed { get; set; } = 5;

        public Texture2D Texture { get; set; }
        public Point Size { get; set; }
        public Point Location { get; set; }
        public Rectangle Rectangle
        {
            get { return new Rectangle(Location, Size); }
            set
            {
                this.Size = value.Size;
                this.Location = value.Location;
            }
        }

        public void Update(Keys left, Keys right, Keys up, Keys down)
        {
            KeyboardState state = Keyboard.GetState();

            if (state.IsKeyDown(up))
            {
                this.Location = new Point(this.Rectangle.X, this.Rectangle.Y - Speed);
            }
            if (state.IsKeyDown(down))
            {
                this.Location = new Point(this.Rectangle.X, this.Rectangle.Y + Speed);
            }
            if (state.IsKeyDown(left))
            {
                this.Location = new Point(this.Rectangle.X - Speed, this.Rectangle.Y);
            }
            if (state.IsKeyDown(right))
            {
                this.Location = new Point(this.Rectangle.X + Speed, this.Rectangle.Y);
            }
        }
    }

}
